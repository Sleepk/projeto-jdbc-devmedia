package br.edu.devmedia.jdbc.bo;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import br.edu.devmedia.jdbc.dao.PessoaDAO;
import br.edu.devmedia.jdbc.dto.EnderecoDTO;
import br.edu.devmedia.jdbc.dto.PessoaDTO;
import br.edu.devmedia.jdbc.exception.NegocioException;
import br.edu.devmedia.jdbc.exception.ValidacaoException;

public class PessoaBO {

	private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

	public void cadastrar(PessoaDTO pessoaDTO) throws NegocioException {
		try {
			PessoaDAO pessoaDAO = new PessoaDAO();
			pessoaDAO.inserir(pessoaDTO);
		} catch (Exception exception) {
			throw new NegocioException(exception.getMessage());
		}
	}

	public void removePessoa(Integer idPessoa) throws NegocioException {
		try {
			PessoaDAO pessoaDAO = new PessoaDAO();
			pessoaDAO.deletar(idPessoa);
		} catch (Exception exception) {
			throw new NegocioException(exception.getMessage());
		}
	}

	public boolean validaNome(String nome) throws ValidacaoException {
		boolean ehValido = true;
		if (nome == null || nome.equals("")) {
			ehValido = false;
			throw new ValidacaoException("Campo nome � obrigat�rio!");
		} else if (nome.length() > 30) {
			ehValido = false;
			throw new ValidacaoException("Campo nome comporta no m�ximo 30 chars!");
		}
		return ehValido;
	}

	public boolean validaCpf(String cpf) throws ValidacaoException {
		boolean ehValido = true;
		if (cpf == null || cpf.equals("")) {
			ehValido = false;
			throw new ValidacaoException("Campo CPF � obrigat�rio!");
		} else if (cpf.length() != 11) {
			ehValido = false;
			throw new ValidacaoException("Campo CPF deve ter 11 d�gitos!");
		} else {
			char[] digitos = cpf.toCharArray();
			for (char digito : digitos) {
				if (!Character.isDigit(digito)) {
					ehValido = false;
					throw new ValidacaoException("Campo CPF � somente num�rico!");
				}
			}
		}
		return ehValido;
	}

	public boolean validaEndereco(String endereco) throws ValidacaoException {
		boolean ehValido = true;
		if (endereco == null || endereco.equals("")) {
			ehValido = false;
			throw new ValidacaoException("Campo Endere�o � obrigat�rio!");
		} else if (endereco.length() > 50) {
			ehValido = false;
			throw new ValidacaoException("Campo Endere�o comporta no m�x. 50 chars!");
		}
		return ehValido;
	}

	public boolean validaDtNasc(String dtNasc) throws ValidacaoException {
		boolean ehValido = true;
		if (dtNasc == null || dtNasc.equals("")) {
			ehValido = false;
			throw new ValidacaoException("Campo Dt. Nasc. � obrigat�rio!");
		} else {
			ehValido = false;
			try {
				dateFormat.parse(dtNasc);
			} catch (ParseException e) {
				throw new ValidacaoException("Formato inv�lido de data!");
			}
		}
		return ehValido;
	}

	public PessoaDTO buscaPorID(Integer idPessoa) throws NegocioException {
		try {
			PessoaDAO pessoaDAO = new PessoaDAO();
			return pessoaDAO.buscarPorId(idPessoa);
		} catch (Exception exception) {
			throw new NegocioException(exception.getMessage());
		}
	}

	public void atualizar(PessoaDTO pessoaDTO) throws NegocioException {
		try {

			PessoaDAO dao = new PessoaDAO();
			dao.atualizar(pessoaDTO);
		} catch (Exception e) {
			e.printStackTrace();
			throw new NegocioException(e.getMessage());
		}
	}

	public boolean validaEndereco(EnderecoDTO enderecoDTO) throws ValidacaoException {
		boolean ehValido = true;
		if (enderecoDTO.getLogradouro() == null || enderecoDTO.getLogradouro().equals("")) {
			ehValido = false;
			throw new ValidacaoException("Campo Rua � obrigat�rio!");
		} else

		if (enderecoDTO.getBairro() == null || enderecoDTO.getBairro().equals("")) {
			ehValido = false;
			throw new ValidacaoException("Campo Bairro � obrigat�rio!");
		} else if (enderecoDTO.getNumero() == null || enderecoDTO.getNumero().equals(0)) {
			ehValido = false;
			throw new ValidacaoException("Campo Numero � obrigat�rio!");
		} else if (enderecoDTO.getCep() == null || enderecoDTO.getCep().equals(0)) {
			ehValido = false;
			throw new ValidacaoException("Campo CEP � obrigat�rio!");
		}

		return ehValido;
	}
}
