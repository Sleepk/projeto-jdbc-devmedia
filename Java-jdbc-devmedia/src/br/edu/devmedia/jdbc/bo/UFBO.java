package br.edu.devmedia.jdbc.bo;

import java.util.List;

import br.edu.devmedia.jdbc.dao.UFDAO;
import br.edu.devmedia.jdbc.dto.UFDTO;
import br.edu.devmedia.jdbc.exception.NegocioException;

public class UFBO {

	public List<UFDTO> listaUfs() throws NegocioException {
		List<UFDTO> lista = null;

		try {
			UFDAO ufdao = new UFDAO();
			lista = ufdao.listaEstados();
		} catch (Exception e) {
			throw new NegocioException(e.getMessage(), e);
		}

		return lista;
	}
}
