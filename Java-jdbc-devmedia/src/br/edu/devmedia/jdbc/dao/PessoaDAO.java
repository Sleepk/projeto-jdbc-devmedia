package br.edu.devmedia.jdbc.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Statement;

import br.edu.devmedia.jdbc.ConexaoUtil;
import br.edu.devmedia.jdbc.dto.EnderecoDTO;
import br.edu.devmedia.jdbc.dto.PessoaDTO;
import br.edu.devmedia.jdbc.dto.UFDTO;
import br.edu.devmedia.jdbc.exception.PersistenciaExcpetion;

public class PessoaDAO implements GenericoDAO<PessoaDTO> {

	@Override
	public void inserir(PessoaDTO pessoaDTO) throws PersistenciaExcpetion {
		try {
			int chaveEnd = inseriEndereco(pessoaDTO.getEnderecoDTO());
			Connection connection = ConexaoUtil.getInstance().getConnection();
			String sql = "INSERT INTO TB_PESSOA(NOME, CPF, SEXO, DT_NASC, cod_endereco) " + "VALUES(?, ?, ?, ?, ?)";

			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, pessoaDTO.getNome());
			statement.setLong(2, pessoaDTO.getCpf());
			statement.setString(3, String.valueOf(pessoaDTO.getSexo()));
			statement.setDate(4, new Date(pessoaDTO.getDtNascimento().getTime()));
			statement.setInt(5, chaveEnd);

			statement.execute();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaExcpetion(e.getMessage(), e);
		}
	}

	private int inseriEndereco(EnderecoDTO enderecoDTO) throws PersistenciaExcpetion {
		int chave = 0;

		try {
			Connection connection = ConexaoUtil.getInstance().getConnection();
			String sql = "INSERT INTO TB_endereco(logradouro, bairro, cidade, numero, cep, cod_uf) "
					+ "VALUES(?, ?, ?, ?, ?, ?)";

			PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, enderecoDTO.getLogradouro());
			statement.setString(2, enderecoDTO.getBairro());
			statement.setString(3, enderecoDTO.getCidade());
			statement.setLong(4, enderecoDTO.getNumero());
			statement.setInt(5, enderecoDTO.getCep());
			statement.setInt(6, enderecoDTO.getUfDTO().getIdUf());

			statement.execute();
			ResultSet result = statement.getGeneratedKeys();

			if (result.next()) {
				chave = result.getInt(1);
			}
			statement.close();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaExcpetion(e.getMessage(), e);
		}
		return chave;
	}

	@Override
	public void atualizar(PessoaDTO pessoaDTO) throws PersistenciaExcpetion {
		try {
			Connection connection = ConexaoUtil.getInstance().getConnection();

			String sql = "UPDATE TB_PESSOA " + " SET NOME = ?, " + " CPF = ?," + " ENDERECO = ?," + " SEXO = ?,"
					+ " DT_NASC = ? " + " WHERE ID_PESSOA = ?";

			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, pessoaDTO.getNome());
			statement.setLong(2, pessoaDTO.getCpf());
			statement.setString(3, pessoaDTO.getEndereco());
			statement.setString(4, String.valueOf(pessoaDTO.getSexo()));
			statement.setDate(5, new Date(pessoaDTO.getDtNascimento().getTime()));
			statement.setInt(6, pessoaDTO.getIdPessoa());

			statement.execute();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaExcpetion(e.getMessage(), e);
		}
	}

	@Override
	public void deletar(Integer idPessoa) throws PersistenciaExcpetion {
		try {
			Connection connection = ConexaoUtil.getInstance().getConnection();

			String sql = "DELETE FROM TB_PESSOA WHERE ID_PESSOA = ?";

			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, idPessoa);

			statement.execute();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaExcpetion(e.getMessage(), e);
		}
	}

	@Override
	public List<PessoaDTO> listarTodos() throws PersistenciaExcpetion {
		List<PessoaDTO> listaPessoas = new ArrayList<PessoaDTO>();
		try {
			Connection connection = ConexaoUtil.getInstance().getConnection();

			String sql = "SELECT * FROM TB_PESSOA";

			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet resultSet = statement.executeQuery();

			while (resultSet.next()) {
				PessoaDTO pessoaDTO = new PessoaDTO();
				pessoaDTO.setIdPessoa(resultSet.getInt("id_pessoa"));
				pessoaDTO.setNome(resultSet.getString("nome"));
				pessoaDTO.setCpf(resultSet.getLong("cpf"));
				pessoaDTO.setDtNascimento(resultSet.getDate("dt_nasc"));
				pessoaDTO.setSexo(resultSet.getString("sexo").charAt(0));
				pessoaDTO.setEnderecoDTO(findAdressByID(resultSet.getInt("cod_endereco")));

				listaPessoas.add(pessoaDTO);
			}
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaExcpetion(e.getMessage(), e);
		}
		return listaPessoas;
	}

	@Override
	public PessoaDTO buscarPorId(Integer id) throws PersistenciaExcpetion {
		PessoaDTO pessoaDTO = null;
		try {
			Connection connection = ConexaoUtil.getInstance().getConnection();

			String sql = "SELECT * FROM TB_PESSOA WHERE ID_PESSOA = ?";

			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, id);

			ResultSet resultSet = statement.executeQuery();
			if (resultSet.next()) {
				pessoaDTO = new PessoaDTO();
				pessoaDTO.setIdPessoa(resultSet.getInt("id_pessoa"));
				pessoaDTO.setNome(resultSet.getString("nome"));
				pessoaDTO.setCpf(resultSet.getLong("cpf"));
				pessoaDTO.setDtNascimento(resultSet.getDate("dt_nasc"));
				pessoaDTO.setEndereco(resultSet.getString("endereco"));
				pessoaDTO.setSexo(resultSet.getString("sexo").charAt(0));
			}
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaExcpetion(e.getMessage(), e);
		}
		return pessoaDTO;
	}

	public List<PessoaDTO> filtraPessoa(PessoaDTO pessoaDTO) throws PersistenciaExcpetion {
		List<PessoaDTO> lista = new ArrayList<PessoaDTO>();

		try {
			Connection connection = ConexaoUtil.getInstance().getConnection();

			String sql = "SELECT * FROM TB_PESSOA ";
			boolean ultimo = false;
			if (pessoaDTO.getNome() != null && !pessoaDTO.getNome().equals("")) {
				sql += " WHERE NOME = ?";
				ultimo = true;
			}

			if (pessoaDTO.getCpf() != null && !pessoaDTO.getCpf().equals("")) {
				if (ultimo) {

					sql += " AND ";
				} else {
					sql += " where ";
					ultimo = true;
				}
				sql += "CPF = ?";
			}

			if (pessoaDTO.getSexo() != null && !pessoaDTO.getSexo().equals("")) {
				if (ultimo) {
					sql += " AND ";

				} else {
					sql += " where ";
				}
				sql += " SEXO = ? ";
			}

			PreparedStatement statement = connection.prepareStatement(sql);

			int cont = 0;

			if (pessoaDTO.getNome() != null && !pessoaDTO.getNome().equals("")) {
				statement.setString(++cont, pessoaDTO.getNome());
			}

			if (pessoaDTO.getCpf() != null && !pessoaDTO.getCpf().equals("")) {
				statement.setLong(++cont, pessoaDTO.getCpf());
			}
			String valueOf = String.valueOf(pessoaDTO.getSexo());
			if (pessoaDTO.getSexo() != null && !pessoaDTO.getSexo().equals("")) {
				statement.setString(++cont, valueOf);
			}

			ResultSet resultSet = statement.executeQuery();

			while (resultSet.next()) {
				pessoaDTO = new PessoaDTO();
				pessoaDTO.setIdPessoa(resultSet.getInt("id_pessoa"));
				pessoaDTO.setNome(resultSet.getString("nome"));
				pessoaDTO.setCpf(resultSet.getLong("cpf"));
				pessoaDTO.setDtNascimento(resultSet.getDate("dt_nasc"));
				pessoaDTO.setEndereco(resultSet.getString("endereco"));
				pessoaDTO.setSexo(resultSet.getString("sexo").charAt(0));

				lista.add(pessoaDTO);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaExcpetion(e.getMessage(), e);
		}
		return lista;
	}

	public EnderecoDTO findAdressByID(Integer idEndereco) throws PersistenciaExcpetion {
		EnderecoDTO enderecoDTO = null;
		try {
			Connection connection = ConexaoUtil.getInstance().getConnection();

			String sql = "select * from tb_endereco where id_endereco = ?";

			PreparedStatement prepareStatement = connection.prepareStatement(sql);
			prepareStatement.setInt(1, idEndereco);

			ResultSet resultSet = prepareStatement.executeQuery();
			if (resultSet.next()) {
				enderecoDTO = new EnderecoDTO();
				enderecoDTO.setBairro(resultSet.getString("bairro"));
				enderecoDTO.setId_endereco(resultSet.getInt("id_endereco"));
				enderecoDTO.setLogradouro(resultSet.getString("logradouro"));
				enderecoDTO.setCidade(resultSet.getString("cidade"));
				enderecoDTO.setNumero(resultSet.getLong("numero"));
				enderecoDTO.setCep(resultSet.getInt("cep"));
				enderecoDTO.setUfDTO(buscaUFPorID(resultSet.getInt("cod_uf")));
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaExcpetion(e.getMessage(), e);
		}
		return enderecoDTO;
	}

	private UFDTO buscaUFPorID(Integer idUF) throws PersistenciaExcpetion {
		UFDTO ufdto = null;

		try {
			Connection connection = ConexaoUtil.getInstance().getConnection();

			String sql = "select * from tb_uf where id_uf = ?";

			PreparedStatement prepareStatement = connection.prepareStatement(sql);
			prepareStatement.setInt(1, idUF);

			ResultSet resultSet = prepareStatement.executeQuery();

			if (resultSet.next()) {
				ufdto = new UFDTO();
				ufdto.setDescricao(resultSet.getString("descricao"));
				ufdto.setIdUf(resultSet.getInt("id_uf"));
				ufdto.setSiglaUF(resultSet.getString("sigla_uf"));

			}
			
		} catch (Exception e) {
			e.printStackTrace();
			throw new PersistenciaExcpetion(e.getMessage(), e);
		}
		return ufdto;
	}

}
