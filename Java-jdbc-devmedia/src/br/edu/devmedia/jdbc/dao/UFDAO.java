package br.edu.devmedia.jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import br.edu.devmedia.jdbc.ConexaoUtil;
import br.edu.devmedia.jdbc.dto.UFDTO;
import br.edu.devmedia.jdbc.exception.PersistenciaExcpetion;

public class UFDAO {

	public List<UFDTO> listaEstados() throws PersistenciaExcpetion {
		List<UFDTO> lista = new ArrayList<UFDTO>();
		try {

			Connection connection = ConexaoUtil.getInstance().getConnection();

			String sql = "select * from tb_uf";
			
			PreparedStatement prepareStatement = connection.prepareStatement(sql);
			ResultSet resultado = prepareStatement.executeQuery();
			
			while(resultado.next()) {
				UFDTO ufdto = new UFDTO();
				
				ufdto.setIdUf(resultado.getInt(1));
				ufdto.setSiglaUF(resultado.getString(2));
				ufdto.setDescricao(resultado.getString(3));
				lista.add(ufdto);
			}
			

		} catch (Exception exception) {

			exception.printStackTrace();
			throw new PersistenciaExcpetion(exception.getMessage(), exception);

		}
		return lista;
	}

}
