package br.edu.devmedia.jdbc.dto;

public class EnderecoDTO {

	private Integer id_endereco;
	private String logradouro;
	private String bairro;
	private String cidade;
	private Long numero;
	private Integer cep;

	private UFDTO ufDTO;

	public Integer getId_endereco() {
		return id_endereco;
	}

	public void setId_endereco(Integer id_endereco) {
		this.id_endereco = id_endereco;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}

	public Integer getCep() {
		return cep;
	}

	public void setCep(Integer cep) {
		this.cep = cep;
	}

	public UFDTO getUfDTO() {
		return ufDTO;
	}

	public void setUfDTO(UFDTO ufDTO) {
		this.ufDTO = ufDTO;
	}

	@Override
	public String toString() {
		return "EnderecoDTO [id_endereco=" + id_endereco + ", logradouro=" + logradouro + ", bairro=" + bairro
				+ ", cidade=" + cidade + ", numero=" + numero + ", cep=" + cep + ", ufDTO=" + ufDTO + "]";
	}

}
