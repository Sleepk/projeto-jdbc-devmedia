package br.edu.devmedia.jdbc.dto;

public class UFDTO {
	private Integer idUf;
	private String siglaUF;

	private String descricao;

	public Integer getIdUf() {
		return idUf;
	}

	public void setIdUf(Integer idUf) {
		this.idUf = idUf;
	}

	public String getSiglaUF() {
		return siglaUF;
	}

	public void setSiglaUF(String siglaUF) {
		this.siglaUF = siglaUF;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	@Override
	public String toString() {
		return "UFDTO [idUf=" + idUf + ", siglaUF=" + siglaUF + ", descricao=" + descricao + "]";
	}
	
	
	
	
}
