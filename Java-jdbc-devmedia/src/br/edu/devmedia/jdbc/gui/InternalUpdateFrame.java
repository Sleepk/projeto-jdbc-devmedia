package br.edu.devmedia.jdbc.gui;

import java.text.SimpleDateFormat;

import javax.swing.ButtonGroup;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import br.edu.devmedia.jdbc.bo.PessoaBO;
import br.edu.devmedia.jdbc.dto.PessoaDTO;
import br.edu.devmedia.jdbc.util.MensagensUtil;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class InternalUpdateFrame extends JInternalFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 696624586218999645L;
	private JTextField txtNome;
	private JTextField txtCPF;
	private JTextField txtEndereco;
	private JTextField txtDtNasc;
	private JRadioButton rbtFeminino;
	private JRadioButton rbtMasculino;
	private ButtonGroup bg = new ButtonGroup();
	private JButton btnSalvar;
	private JLabel lblId;

	/**
	 * Create the frame.
	 */
	public InternalUpdateFrame() {
		setTitle("Editar Pessoa");

		setBounds(100, 100, 450, 179);
		getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 414, 128);
		getContentPane().add(panel);
		panel.setLayout(null);

		JLabel lblNome = new JLabel("Nome:");
		lblNome.setBounds(6, 31, 46, 14);
		panel.add(lblNome);

		txtNome = new JTextField();
		txtNome.setColumns(10);
		txtNome.setBounds(53, 28, 86, 20);
		panel.add(txtNome);

		JLabel lblCPF = new JLabel("CPF");
		lblCPF.setBounds(6, 59, 46, 14);
		panel.add(lblCPF);

		txtCPF = new JTextField();
		txtCPF.setColumns(10);
		txtCPF.setBounds(53, 56, 86, 20);
		panel.add(txtCPF);

		JLabel lblEndereco = new JLabel("Endereco");
		lblEndereco.setBounds(149, 31, 54, 14);
		panel.add(lblEndereco);

		txtEndereco = new JTextField();
		txtEndereco.setColumns(10);
		txtEndereco.setBounds(272, 28, 86, 20);
		panel.add(txtEndereco);

		JLabel lblDtNascimento = new JLabel("Data Nascimento");
		lblDtNascimento.setBounds(149, 59, 105, 14);
		panel.add(lblDtNascimento);

		txtDtNasc = new JTextField();
		txtDtNasc.setColumns(10);
		txtDtNasc.setBounds(272, 56, 86, 20);
		panel.add(txtDtNasc);

		rbtMasculino = new JRadioButton("Masculino");
		rbtMasculino.setBounds(6, 89, 86, 23);
		panel.add(rbtMasculino);

		rbtFeminino = new JRadioButton("Feminino");
		rbtFeminino.setBounds(96, 89, 76, 23);
		panel.add(rbtFeminino);

		bg.add(rbtFeminino);
		bg.add(rbtMasculino);

		btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {

					SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

					String nome = txtNome.getText();
					String cpf = txtCPF.getText();
					String endereco = txtEndereco.getText();
					String nasc = txtDtNasc.getText();

					PessoaBO pessoaBO = new PessoaBO();
					pessoaBO.validaNome(nome);
					pessoaBO.validaCpf(cpf);
					pessoaBO.validaEndereco(endereco);
					pessoaBO.validaDtNasc(nasc);

					PessoaDTO pessoaDTO = new PessoaDTO();
					pessoaDTO.setNome(nome);
					pessoaDTO.setIdPessoa(Integer.parseInt(lblId.getText()));
					pessoaDTO.setEndereco(endereco);
					pessoaDTO.setCpf(Long.parseLong(cpf));

					pessoaDTO.setDtNascimento(dateFormat.parse(nasc));
					char sexo = rbtMasculino.isSelected() ? 'M' : 'F';
					pessoaDTO.setSexo(sexo);

					pessoaBO.atualizar(pessoaDTO);
					MensagensUtil.addMsg(InternalUpdateFrame.this, "Altera��o Salva com sucesso");

					setVisible(false);
				} catch (Exception exception) {
					exception.printStackTrace();
					MensagensUtil.addMsg(InternalUpdateFrame.this, exception.getMessage());
				}

			}
		});
		btnSalvar.setBounds(269, 88, 89, 23);
		panel.add(btnSalvar);

		lblId = new JLabel("id");
		lblId.setVisible(false);
		lblId.setBounds(6, 6, 46, 14);
		panel.add(lblId);

	}

	public void carregaItens(PessoaDTO pessoaDTO) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		this.lblId.setText((pessoaDTO.getIdPessoa().toString()));
		this.txtCPF.setText(pessoaDTO.getCpf().toString());
		this.txtNome.setText(pessoaDTO.getNome());
		this.txtEndereco.setText(pessoaDTO.getEndereco());
		this.txtDtNasc.setText(dateFormat.format(pessoaDTO.getDtNascimento()));
		this.rbtMasculino.setSelected((pessoaDTO.getSexo()) == 'M');
		this.rbtFeminino.setSelected((pessoaDTO.getSexo()) == 'F');
	}
}
