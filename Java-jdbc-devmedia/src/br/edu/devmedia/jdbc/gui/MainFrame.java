package br.edu.devmedia.jdbc.gui;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import br.edu.devmedia.jdbc.bo.PessoaBO;
import br.edu.devmedia.jdbc.bo.UFBO;
import br.edu.devmedia.jdbc.dao.PessoaDAO;
import br.edu.devmedia.jdbc.dto.EnderecoDTO;
import br.edu.devmedia.jdbc.dto.PessoaDTO;
import br.edu.devmedia.jdbc.dto.UFDTO;
import br.edu.devmedia.jdbc.exception.NegocioException;
import br.edu.devmedia.jdbc.exception.PersistenciaExcpetion;
import br.edu.devmedia.jdbc.util.MensagensUtil;
import javax.swing.ScrollPaneConstants;

public class MainFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3639045345781271113L;
	private JPanel contentPane;
	private final ButtonGroup rbtGroup = new ButtonGroup();
	private JTextField txtNomeConsulta;
	private JTextField txtCPFConsulta;
	private JTextField txtNome;
	private PessoaJTable tblPessoa;
	private PessoaJTable pessoaJTable;
	private InternalUpdateFrame internalUpdateFrame;
	private JComboBox<UFBO> jComboUF;
	private JTextField txtLogradouro;
	private JTextField txtBairro;
	private JTextField txtCidade;
	private JTextField txtCEP;
	private JTextField txtNumero;
	private JPanel pnlDadosPessoais;
	private JTextField txtCPF;
	private JTextField txtDtNasc;
	private JRadioButton rbtMasculino;
	private JLabel lblNumero;
	private Component lblCep;
	private JLabel lblEstado;
	private JLabel lblDataNascimento;
	private JRadioButton rbtFeminino;
	private JLabel lblCpf;
	private JLabel lblNome;
	private JPanel pnlListagem;
	private JButton button;
	private JScrollPane scrollPane;
	private JLabel lblSexo;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 933, 459);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 11, 882, 398);
		contentPane.add(tabbedPane);

		JPanel pnlCadastro = new JPanel();
		pnlCadastro.setBorder(
				new TitledBorder(null, "Cadastro de Cliente", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		tabbedPane.addTab("Cadastro", null, pnlCadastro, null);
		pnlCadastro.setVisible(false);

		JButton btnCadastrar = new JButton("Cadastrar");
		btnCadastrar.setBounds(301, 287, 141, 23);
		btnCadastrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PessoaDTO pessoaDTO = new PessoaDTO();
				EnderecoDTO enderecoDTO = new EnderecoDTO();
				PessoaBO pessoaBO = new PessoaBO();
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				try {
					String nome = txtNome.getText();
					String cpf = txtCPF.getText();
					String endereco = txtLogradouro.getText();
					String nasc = txtDtNasc.getText();

					pessoaBO.validaNome(nome);
					pessoaBO.validaCpf(cpf);
					pessoaBO.validaEndereco(endereco);
					pessoaBO.validaDtNasc(nasc);

					pessoaDTO.setNome(nome);
					pessoaDTO.setEndereco(endereco);
					pessoaDTO.setCpf(Long.parseLong(cpf));

					pessoaDTO.setDtNascimento(dateFormat.parse(nasc));
					char sexo = rbtMasculino.isSelected() ? 'M' : 'F';
					pessoaDTO.setSexo(sexo);

					enderecoDTO.setLogradouro(txtLogradouro.getText());
					enderecoDTO.setBairro(txtBairro.getText());
					enderecoDTO.setCep(txtCEP.getText().equals("") ? null : Integer.parseInt(txtCEP.getText()));
					enderecoDTO.setCidade(txtCidade.getText());
					enderecoDTO.setNumero(txtNumero.getText().equals("") ? null : Long.parseLong(txtNumero.getText()));

					// valida os dados do endereco
					pessoaBO.validaEndereco(enderecoDTO);

					UFDTO ufdto = new UFDTO();
					ufdto.setIdUf(jComboUF.getSelectedIndex() + 1);
					enderecoDTO.setUfDTO(ufdto);

					pessoaDTO.setEnderecoDTO(enderecoDTO);

					pessoaBO.cadastrar(pessoaDTO);
					MensagensUtil.addMsg(MainFrame.this, "Cadastro efetuado com sucesso!");

					MainFrame.this.dispose();
					main(null);
				} catch (Exception evento) {
					evento.printStackTrace();
					MensagensUtil.addMsg(MainFrame.this, evento.getMessage());
				}
			}
		});
		pnlCadastro.setLayout(null);
		pnlCadastro.add(btnCadastrar);

		JButton btnLimpar = new JButton("Limpar");
		btnLimpar.setBounds(533, 287, 89, 23);
		pnlCadastro.add(btnLimpar);

		JPanel pnlEndereco = new JPanel();
		pnlEndereco.setBounds(301, 21, 321, 239);
		pnlEndereco.setBorder(new TitledBorder(null, "Endere�o", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlCadastro.add(pnlEndereco);
		pnlEndereco.setLayout(null);

		JLabel lblLogradouro = new JLabel("Logradouro");
		lblLogradouro.setBounds(12, 26, 92, 14);
		pnlEndereco.add(lblLogradouro);

		txtLogradouro = new JTextField();
		txtLogradouro.setColumns(10);
		txtLogradouro.setBounds(127, 23, 86, 20);
		pnlEndereco.add(txtLogradouro);

		JLabel lblBairro = new JLabel("Bairro");
		lblBairro.setBounds(12, 55, 46, 14);
		pnlEndereco.add(lblBairro);

		txtBairro = new JTextField();
		txtBairro.setColumns(10);
		txtBairro.setBounds(127, 52, 86, 20);
		pnlEndereco.add(txtBairro);

		JLabel lblCidade = new JLabel("Cidade");
		lblCidade.setBounds(12, 84, 46, 14);
		pnlEndereco.add(lblCidade);

		txtCidade = new JTextField();
		txtCidade.setColumns(10);
		txtCidade.setBounds(127, 81, 86, 20);
		pnlEndereco.add(txtCidade);

		txtCEP = new JTextField();
		txtCEP.setColumns(10);
		txtCEP.setBounds(127, 139, 86, 20);
		pnlEndereco.add(txtCEP);

		lblNumero = new JLabel("Numero");
		lblNumero.setBounds(12, 113, 46, 14);
		pnlEndereco.add(lblNumero);

		lblCep = new JLabel("CEP");
		lblCep.setBounds(12, 142, 46, 14);
		pnlEndereco.add(lblCep);

		txtNumero = new JTextField();
		txtNumero.setColumns(10);
		txtNumero.setBounds(127, 110, 86, 20);
		pnlEndereco.add(txtNumero);

		lblEstado = new JLabel("Estado");
		lblEstado.setBounds(12, 175, 46, 14);
		pnlEndereco.add(lblEstado);

		comboEstado(pnlEndereco);

		pnlDadosPessoais = new JPanel();
		pnlDadosPessoais.setBounds(8, 21, 281, 148);
		pnlDadosPessoais
				.setBorder(new TitledBorder(null, "JPanel title", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlCadastro.add(pnlDadosPessoais);
		pnlDadosPessoais.setLayout(null);

		lblDataNascimento = new JLabel("Data Nascimento");
		lblDataNascimento.setBounds(22, 80, 97, 14);
		pnlDadosPessoais.add(lblDataNascimento);

		txtCPF = new JTextField();
		txtCPF.setColumns(10);
		txtCPF.setBounds(163, 52, 86, 20);
		pnlDadosPessoais.add(txtCPF);

		txtDtNasc = new JTextField();
		txtDtNasc.setColumns(10);
		txtDtNasc.setBounds(163, 77, 86, 20);
		pnlDadosPessoais.add(txtDtNasc);

		rbtFeminino = new JRadioButton("Feminino");
		rbtGroup.add(rbtFeminino);
		rbtFeminino.setBounds(173, 99, 76, 23);
		pnlDadosPessoais.add(rbtFeminino);

		rbtMasculino = new JRadioButton("Masculino");
		rbtGroup.add(rbtMasculino);
		rbtMasculino.setSelected(true);
		rbtMasculino.setBounds(83, 99, 86, 23);
		pnlDadosPessoais.add(rbtMasculino);

		lblCpf = new JLabel("CPF");
		lblCpf.setBounds(22, 54, 105, 14);
		pnlDadosPessoais.add(lblCpf);

		lblNome = new JLabel("Nome");
		lblNome.setBounds(22, 28, 46, 14);
		pnlDadosPessoais.add(lblNome);

		txtNome = new JTextField();
		txtNome.setColumns(10);
		txtNome.setBounds(163, 23, 86, 20);
		pnlDadosPessoais.add(txtNome);

		lblSexo = new JLabel("Sexo");
		lblSexo.setBounds(22, 102, 40, 16);
		pnlDadosPessoais.add(lblSexo);

		pnlListagem = new JPanel();
		tabbedPane.addTab("Listagem", null, pnlListagem, null);
		pnlListagem.setLayout(null);

		button = new JButton("Atualizar");
		button.setBounds(778, 1, 89, 23);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				carregarTabela();
			}
		});
		pnlListagem.add(button);

		scrollPane = new JScrollPane();
		scrollPane.setBounds(12, 35, 855, 115);
		pnlListagem.add(scrollPane);

		tblPessoa = new PessoaJTable();
		scrollPane.setViewportView(tblPessoa);

		JPopupMenu popupMenu = new JPopupMenu();
		addPopup(tblPessoa, popupMenu);

		JMenuItem mitemNovo = new JMenuItem("Cadastrar");
		popupMenu.add(mitemNovo);

		JMenuItem mntmVisualizarCadastro = new JMenuItem("Visualizar Cadastro");
		mntmVisualizarCadastro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				PessoaDTO motoristaSelecionado = tblPessoa.getPessoaSelecionada();
				JOptionPane.showMessageDialog(MainFrame.this, motoristaSelecionado);
			}
		});
		popupMenu.add(mntmVisualizarCadastro);

		JMenuItem mntmEditar = new JMenuItem("Editar");
		mntmEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evento) {
				try {
					PessoaBO pessoaBO = new PessoaBO();
					PessoaDTO motoristaSelecionado = tblPessoa.getPessoaSelecionada();
					motoristaSelecionado = pessoaBO.buscaPorID(motoristaSelecionado.getIdPessoa());
					populaInternalFrame(motoristaSelecionado, internalUpdateFrame);
				} catch (NegocioException exception) {
					exception.printStackTrace();
				}
			}
		});
		popupMenu.add(mntmEditar);

		JMenuItem mntmDeletar = new JMenuItem("Deletar");
		mntmDeletar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evento) {

				PessoaDTO motoristaSelecionado = tblPessoa.getPessoaSelecionada();

				int resposta = JOptionPane.showConfirmDialog(MainFrame.this,
						"Deseja realmente deletar esta pessoa: " + motoristaSelecionado.getNome() + "?");
				if (resposta == 0) {
					PessoaBO pessoaBO = new PessoaBO();
					try {
						pessoaBO.removePessoa(motoristaSelecionado.getIdPessoa());
						carregarTabela();
						MensagensUtil.addMsg(MainFrame.this,
								"Pessoa " + motoristaSelecionado.getNome() + " removida com sucesso");

					} catch (NegocioException exception) {
						exception.printStackTrace();
						MensagensUtil.addMsg(MainFrame.this, exception.getMessage());
					}
				}
			}
		});
		popupMenu.add(mntmDeletar);

		JPanel pnlBusca = new JPanel();
		tabbedPane.addTab("Consulta Personalizada", null, pnlBusca, null);
		pnlBusca.setLayout(null);

		JLabel label = new JLabel("Nome:");
		label.setBounds(12, 15, 46, 14);
		pnlBusca.add(label);

		txtNomeConsulta = new JTextField();
		txtNomeConsulta.setBounds(76, 12, 86, 20);
		txtNomeConsulta.setColumns(10);
		pnlBusca.add(txtNomeConsulta);

		JLabel label_1 = new JLabel("CPF");
		label_1.setBounds(180, 15, 46, 14);
		pnlBusca.add(label_1);

		txtCPFConsulta = new JTextField();
		txtCPFConsulta.setBounds(244, 12, 86, 20);
		txtCPFConsulta.setColumns(10);
		pnlBusca.add(txtCPFConsulta);

		JRadioButton rbtMascConsulta = new JRadioButton("Masculino");
		rbtGroup.add(rbtMascConsulta);
		rbtMascConsulta.setSelected(true);
		rbtMascConsulta.setBounds(49, 37, 109, 23);
		pnlBusca.add(rbtMascConsulta);

		JRadioButton rbtFemConsulta = new JRadioButton("Feminino");
		rbtGroup.add(rbtFemConsulta);
		rbtFemConsulta.setBounds(190, 37, 109, 23);
		pnlBusca.add(rbtFemConsulta);

		JPanel panel = new JPanel();
		panel.setBounds(7, 61, 482, 143);
		pnlBusca.add(panel);
		panel.setLayout(null);

		JPanel pnlTabelaConsultaPersonalizada = new JPanel();
		pnlTabelaConsultaPersonalizada.setBounds(0, 0, 482, 143);
		panel.add(pnlTabelaConsultaPersonalizada);
		pnlTabelaConsultaPersonalizada
				.setBorder(new TitledBorder(null, "Dados:", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		pnlTabelaConsultaPersonalizada.setLayout(null);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(12, 31, 458, 100);
		pnlTabelaConsultaPersonalizada.add(scrollPane_1);

		pessoaJTable = new PessoaJTable();
		pessoaJTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		pessoaJTable.setShowHorizontalLines(false);
		scrollPane_1.setViewportView(pessoaJTable);

		carregarTabela();

		JPanel panelInternalFrame = new JPanel();
		panelInternalFrame.setBounds(12, 160, 855, 199);
		panelInternalFrame.setLayout(null);
		internalUpdateFrame = new InternalUpdateFrame();
		internalUpdateFrame.setEnabled(false);
		internalUpdateFrame.setBounds(12, 12, 833, 168);
		panelInternalFrame.add(internalUpdateFrame);
		pnlListagem.add(panelInternalFrame);

		JButton btnConsultar = new JButton("Consultar");
		btnConsultar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					String nome = txtNomeConsulta.getText();
					Long cpf = txtCPFConsulta.getText().equals("") ? null : Long.parseLong(txtCPFConsulta.getText());
					char sexo = rbtMascConsulta.isSelected() ? 'M' : 'F';

					PessoaDTO pessoaDTO = new PessoaDTO(nome, cpf, sexo);

					carregarTabelaPersonalizada(pessoaJTable, pessoaDTO);
				} catch (Exception evento) {
					evento.printStackTrace();
					MensagensUtil.addMsg(MainFrame.this, evento.getMessage());

				}

			}
		});
		btnConsultar.setBounds(366, 23, 98, 26);
		pnlBusca.add(btnConsultar);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void comboEstado(JPanel pnlEndereco) {
		UFBO ufbo = new UFBO();
		jComboUF = new JComboBox<UFBO>();
		jComboUF.setBounds(127, 170, 132, 25);
		pnlEndereco.add(jComboUF);
		try {
			jComboUF.setModel(new DefaultComboBoxModel(converterEstados(ufbo.listaUfs())));
		} catch (NegocioException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		jComboUF.setSelectedIndex(0);
	}

	public void carregarTabela() {
		try {
			tblPessoa.load(new PessoaDAO().listarTodos());
		} catch (PersistenciaExcpetion e) {
			e.printStackTrace();
			MensagensUtil.addMsg(MainFrame.this, "Ocorreu um erro ao carregar a tabela: " + e.getMessage());
		}
	}

	private void carregarTabelaPersonalizada(PessoaJTable tabela, PessoaDTO pessoaDTO) {
		try {
			tabela.load(new PessoaDAO().filtraPessoa(pessoaDTO));

		} catch (PersistenciaExcpetion e) {
			e.printStackTrace();
			MensagensUtil.addMsg(MainFrame.this, "Ocorreu um ao realizar a consulta: " + e.getMessage());
		}
	}

	private static void addPopup(Component component, final JPopupMenu popup) {
		component.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			public void mouseReleased(MouseEvent e) {
				if (e.isPopupTrigger()) {
					showMenu(e);
				}
			}

			private void showMenu(MouseEvent e) {
				popup.show(e.getComponent(), e.getX(), e.getY());
			}
		});
	}

	private void populaInternalFrame(PessoaDTO pessoaDTO, InternalUpdateFrame internalUpdateFrame) {
		internalUpdateFrame.carregaItens(pessoaDTO);
		internalUpdateFrame.setVisible(true);
	}

	/**
	 * Funcao para criar uma lista suspensa com itens vindo de um banco de dados.
	 * 
	 * @param lista
	 * @return
	 */
	public String[] converterEstados(List<UFDTO> lista) {
		String[] result = new String[lista.size()];

		for (int i = 0; i < result.length; i++) {
			UFDTO ufdto = lista.get(i);
			result[i] = ufdto.getDescricao();
		}

		return result;
	}
}
