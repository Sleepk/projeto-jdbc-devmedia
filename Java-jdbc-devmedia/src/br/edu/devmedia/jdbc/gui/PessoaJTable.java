package br.edu.devmedia.jdbc.gui;

import java.util.List;

import javax.swing.JTable;

import br.edu.devmedia.jdbc.dto.PessoaDTO;

public class PessoaJTable extends JTable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5375656186485851877L;

	private PessoaTableModel tableModel;

	public PessoaJTable() {
		this.tableModel = new  PessoaTableModel();
		setModel(this.tableModel);
	}

	public void load(List<PessoaDTO> pessoas) {
		this.tableModel.load(pessoas);
		this.setAutoCreateRowSorter(true);
	}
	
	
	
	public PessoaDTO getPessoaSelecionada() {
		int index = getSelectedRow();
		return this.tableModel.getPessoaAt(index);
	}

}
