package br.edu.devmedia.jdbc.gui;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

import br.edu.devmedia.jdbc.dto.PessoaDTO;

public class PessoaTableModel extends AbstractTableModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8729841779928555161L;

	private static SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
	private List<PessoaDTO> pessoas = new ArrayList<>();

	// Onde sera carregado a lista e quando houver alguma modificacao, havera
	// alteracao
	public void load(List<PessoaDTO> pessoas) {
		this.pessoas = pessoas;
		fireTableDataChanged();

	}

	/**
	 * Quantidade de colunas a ser exibido
	 */
	@Override
	public int getColumnCount() {
		return 8;
	}

	/**
	 * Pegara as informacoes do tamanho da lista
	 */
	@Override
	public int getRowCount() {
		return pessoas.size();
	}

	/**
	 * Verifica o tamanho de pessoaDTO
	 * 
	 * @param index
	 * @return
	 */
	public PessoaDTO getPessoaAt(int index) {
		if (this.pessoas.size() <= 0) {
			System.out.println("Nao ha dados para exibir");
			return null;
		}
		return this.pessoas.get(index);
	}

	/**
	 * Campos para ser exibido
	 */
	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		if (this.pessoas.size() <= 0) {
			JOptionPane.showMessageDialog(null, "Nao ha pessoas para exibir");
			return null;
		}

		PessoaDTO pessoa = pessoas.get(rowIndex);
		switch (columnIndex) {
		case 0:
			return pessoa.getNome();
		case 1:
			return pessoa.getCpf();

		case 2:
			return pessoa.getEndereco();
		case 3:
			if (pessoa.getDtNascimento() == null) {
				return pessoa.getDtNascimento();
			} else {
				return dateformat.format(pessoa.getDtNascimento());
			}
		case 4:
			return pessoa.getSexo() == 'M' ? "Masculino" : "Feminino";

		case 5:
			return pessoa.getEnderecoDTO().getLogradouro();

		case 6:
			return  pessoa.getEnderecoDTO().getCep();
					
		case 7:
			return pessoa.getEnderecoDTO().getUfDTO().getDescricao();
		}
		return null;
	}

	/**
	 * Nome das Colunas
	 */
	@Override
	public String getColumnName(int columnIndex) {

		switch (columnIndex) {
		case 0:
			return "Nome";
		case 1:
			return "CPF";
		case 2:
			return "Endereco";
		case 3:
			return "Data Nascimento";
		case 4:
			return "Sexo";

		case 5:
			return "Logradouro";

		case 6:
			return "CEP";
		case 7:
			return "Estado";
		}
		return null;
	}

}